import { Routes } from '@angular/router';
import { combineLatestInit } from 'rxjs/internal/observable/combineLatest';

import { ListarCategoriaComponent } from './listar';
import { CadastrarCategoriaComponent } from './cadastrar';
import { EditarCategoriaComponent } from './editar';

export const CategoriaRouts: Routes = [
    {
        path: 'categorias',
        redirectTo: 'categorias/listar'
    },
    {
        path: 'categorias/listar',
        component: ListarCategoriaComponent
    },
    {
        path: 'categorias/cadastrar',
        component: CadastrarCategoriaComponent
    },
    {
        path: 'categorias/editar/:id',
        component: EditarCategoriaComponent
    },
];

