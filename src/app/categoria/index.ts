export * from './categoria.module';
export * from './shared';
export * from './listar';
export * from './cadastrar';
export * from './editar';
export * from './categoria-routing.module';
