import { Component, OnInit, ViewChild } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { NgForm } from '@angular/forms';
import { Observable } from 'rxjs';

import { CategoriaService, Categoria } from '../shared';

@Component({
  selector: 'app-editar-categoria',
  templateUrl: './editar-categoria.component.html',
  styleUrls: ['./editar-categoria.component.css']
})
export class EditarCategoriaComponent implements OnInit {

  @ViewChild('formCategoria', {static: true}) formCategoria: NgForm;
  categoria: Categoria;

  constructor(
    private categoriaService: CategoriaService,
    private route: ActivatedRoute,
    private router: Router
  ) { }

  ngOnInit(): void {
    this.categoria = new Categoria;
    const id = +this.route.snapshot.params['id'];        
    this.getCategoriaPorId(id);    
  }

  getCategoriaPorId(id: number){
    this.categoriaService.getCategoriaId(id).subscribe((categoria: Categoria[]) => {      
      if (categoria['code'] == 200) {        
        this.categoria = categoria['data'][0];  
      } 
    });    
  }

  saveCategoria() {
    if (this.categoria.id !== undefined) {
      this.categoriaService.updateCategoria(this.categoria).subscribe(() => {        
      });
    } else {
      this.categoriaService.saveCategoria(this.categoria).subscribe(() => {        
      });
    }
  }

  atualizar(): void {
    if (this.formCategoria.form.valid) {
      this.saveCategoria();   
      this.router.navigate(["/categorias"]);
    }
  }

}
