import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { RouterModule } from '@angular/router';
import { FormsModule } from '@angular/forms';

import { HttpClientModule } from '@angular/common/http';
import { CategoriaService } from './shared';
import { ListarCategoriaComponent } from './listar';
import { CadastrarCategoriaComponent } from './cadastrar';
import { EditarCategoriaComponent } from './editar';



@NgModule({  
  declarations: [
    ListarCategoriaComponent,
    CadastrarCategoriaComponent,
    EditarCategoriaComponent
  ],
  imports: [
    CommonModule,
    RouterModule,
    FormsModule,
    HttpClientModule
  ],  
  providers:[
    CategoriaService
  ]
})
export class CategoriaModule { }
