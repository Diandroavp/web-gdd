import { Component, OnInit } from '@angular/core';
import { Observable } from 'rxjs';
import { CategoriaService, Categoria } from '..';


@Component({
  selector: 'app-listar-categoria',
  templateUrl: './listar-categoria.component.html',
  styleUrls: ['./listar-categoria.component.css']
})
export class ListarCategoriaComponent implements OnInit {

  categorias: Categoria[];

  constructor(private categoriaService: CategoriaService) { }

  ngOnInit(): void {           
    this.getCategoria();
  }

  
  getCategoria(){        
    this.categoriaService.getCategoria().subscribe((categoria: Categoria[]) => {      
      if (categoria['code'] == 200) {        
        this.categorias = categoria['data'];        
      } else {
        console.log('code: ', categoria['code']);
        this.categorias = [];
      }
    });    
  }

  remover($event: any, categoria: Categoria): void {
    $event.preventDefault();
    if (confirm('Confirma a remoção do cadastro "' +  categoria.name + '"?')) {      
      this.categoriaService.deleteCategoria(categoria).subscribe(() => {
        this.getCategoria();
      });      
    }
  }


  /*********** ACESSO EXTERNO */
  /*
  listarTodos(): Categoria[] {
    return this.categoriaService.listarTodos();
    this.getCategoria();    
    return this.categorias;
        
  }
*/
/*
  remover($event: any, categoria: Categoria): void {
    $event.preventDefault();
    if (confirm('Confirma a remoção do cadastro "' +  categoria.name + '"?')) {
      this.categoriaService.remover(categoria.id);
      this.getCategoria();
    }
  }
*/
}
