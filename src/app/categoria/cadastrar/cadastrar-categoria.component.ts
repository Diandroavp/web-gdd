import { Component, OnInit, ViewChild } from '@angular/core';
import { Router } from '@angular/router';
import { NgForm } from '@angular/forms';

import { CategoriaService, Categoria } from '../shared';

@Component({
  selector: 'app-cadastrar-categoria',
  templateUrl: './cadastrar-categoria.component.html',
  styleUrls: ['./cadastrar-categoria.component.css']
})
export class CadastrarCategoriaComponent implements OnInit {

  @ViewChild('formCategoria', {static: true}) formCategoria: NgForm;
  categoria: Categoria;

  constructor(
    private categoriaService: CategoriaService,
    private router: Router
  ) { }

  ngOnInit(): void {
    this.categoria   =  new Categoria;
  }

  saveCategoria() {
    if (this.categoria.id !== undefined) {
      this.categoriaService.updateCategoria(this.categoria).subscribe(() => {        
      });
    } else {
      this.categoriaService.saveCategoria(this.categoria).subscribe(() => {        
      });
    }
  }
  
  cadastrar(): void {
    if (this.formCategoria.form.valid) {      
      this.saveCategoria();
      this.router.navigate(["/categorias"]);
    }
  }
  

}
