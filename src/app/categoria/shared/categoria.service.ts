import { Injectable } from '@angular/core';
import { HttpClient, HttpErrorResponse, HttpHeaders } from '@angular/common/http';
import { Observable, throwError } from 'rxjs';
import { retry, catchError } from 'rxjs/operators';

import { Categoria } from './';

@Injectable({
  providedIn: 'root'
})
export class CategoriaService {

  //url = 'http://localhost:8080/app/';
  url = 'https://api-gdd-principal-dot-teste-diandro.uc.r.appspot.com/app/';

  categoria: Categoria;

  constructor(private httpClient: HttpClient) { }

   // Headers
  httpOptions = {
    headers: new HttpHeaders({ 
      'Content-Type': 'application/json',
      'Access-Control-Allow-Origin': '*',
      'Access-Control-Allow-Headers': 'Content-Type',
      'Access-Control-Allow-Methods': 'GET,POST,OPTIONS,DELETE,PUT',
    })
  }
  

  getCategoria(): Observable<Categoria[]> {
    return this.httpClient.get<Categoria[]>(this.url + 'list_category')
      .pipe(
        retry(2),
        catchError(this.handleError));
  }

  getCategoriaId(id: number): Observable<Categoria[]> {
    return this.httpClient.post<Categoria[]>(this.url + 'category_id', JSON.stringify({"id":id}), this.httpOptions)
      .pipe(
        retry(2),
        catchError(this.handleError)
      )
  }

  saveCategoria(categoria: Categoria): Observable<Categoria> {
    return this.httpClient.post<Categoria>(this.url + 'insert_category', JSON.stringify(categoria), this.httpOptions)
      .pipe(
        retry(2),
        catchError(this.handleError)
      )
  }

  deleteCategoria(categoria: Categoria) {
    return this.httpClient.post<Categoria>(this.url + 'delete_category', JSON.stringify(categoria), this.httpOptions)
      .pipe(
        retry(1),
        catchError(this.handleError)
      )
  }

  updateCategoria(categoria: Categoria): Observable<Categoria> {
    return this.httpClient.post<Categoria>(this.url + 'update_category', JSON.stringify(categoria), this.httpOptions)
      .pipe(
        retry(1),
        catchError(this.handleError)
      )
  }

  

  // Manipulação de erros
  handleError(error: HttpErrorResponse) {
    let errorMessage = '';
    if (error.error instanceof ErrorEvent) {
      // Erro ocorreu no lado do client
      errorMessage = error.error.message;
    } else {
      // Erro ocorreu no lado do servidor
      errorMessage = `Código do erro: ${error.status}, ` + `menssagem: ${error.message}`;
    }
    console.log(errorMessage);
    return throwError(errorMessage);
  };
  
  /************************************************************************************** */
  
  listarTodos(): Categoria[] {
    const categorias = localStorage['categorias'];
    return categorias ? JSON.parse(categorias) : [];
  }
  

  cadastrar(categoria: Categoria): void {
    const categorias = this.listarTodos();
    categoria.id = new Date().getTime();
    categorias.push(categoria);
    localStorage['categorias'] = JSON.stringify(categorias);
  }

  buscarPorId(id: number): Categoria {
    const categorias: Categoria[] = this.listarTodos();
    return categorias.find(categoria => categoria.id === id);    
  }

  atualizar(categoria: Categoria): void {
    console.log('atualizar');
    const categorias: Categoria[] = this.listarTodos();
    categorias.forEach((obj, index, objs) =>{      
      if (categoria.id === obj.id){        
        objs[index] = categoria;
      }
    });
    localStorage['categorias'] = JSON.stringify(categorias);

  }

  remover(id: number): void {
    console.log('remover');
    let categorias: Categoria[] = this.listarTodos();
    categorias = categorias.filter(categoria => categoria.id !== id);
    localStorage['categorias'] = JSON.stringify(categorias)
    
  }
  
}
