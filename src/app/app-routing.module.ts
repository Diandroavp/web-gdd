import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';

import { CategoriaRouts } from './categoria';
import { DispositivoRouts } from './dispositivo';

const routes: Routes = [
  {
    path: '',
    redirectTo: '/categorias/listar',
    pathMatch: 'full'
  },
  ...CategoriaRouts,
  ...DispositivoRouts
];

@NgModule({
  imports: [RouterModule.forRoot(routes, { relativeLinkResolution: 'legacy' })],
  exports: [RouterModule]
})
export class AppRoutingModule { }
