import { Injectable } from '@angular/core';
import { HttpClient, HttpErrorResponse, HttpHeaders } from '@angular/common/http';
import { Observable, throwError } from 'rxjs';
import { retry, catchError } from 'rxjs/operators';

import { Dispositivo } from './';
import { Categoria } from '../../categoria';


@Injectable({
  providedIn: 'root'
})
export class DispositivoService {

  //url = 'http://localhost:8080/app/';
  url = 'https://api-gdd-principal-dot-teste-diandro.uc.r.appspot.com/app/';

  dispositivo: Dispositivo;
  categoria: Categoria;

  constructor(private httpClient: HttpClient) { }

   // Headers
  httpOptions = {
    headers: new HttpHeaders({ 
      'Content-Type': 'application/json',
      'Access-Control-Allow-Origin': '*',
      'Access-Control-Allow-Headers': 'Content-Type',
      'Access-Control-Allow-Methods': 'GET,POST,OPTIONS,DELETE,PUT',
    })
  }
  

  getDispositivo(): Observable<Dispositivo[]> {
    return this.httpClient.get<Dispositivo[]>(this.url + 'list_device')
      .pipe(
        retry(2),
        catchError(this.handleError));
  }

  getDispositivoId(id: number): Observable<Dispositivo[]> {
    return this.httpClient.post<Dispositivo[]>(this.url + 'device_id', JSON.stringify({"id":id}), this.httpOptions)
      .pipe(
        retry(2),
        catchError(this.handleError)
      )
  }

  saveDispositivo(dispositivo: Dispositivo): Observable<Dispositivo> {
    return this.httpClient.post<Dispositivo>(this.url + 'insert_device', JSON.stringify(dispositivo), this.httpOptions)
      .pipe(
        retry(2),
        catchError(this.handleError)
      )
  }

  deleteDispositivo(dispositivo: Dispositivo) {
    return this.httpClient.post<Dispositivo>(this.url + 'delete_device', JSON.stringify(dispositivo), this.httpOptions)
      .pipe(
        retry(1),
        catchError(this.handleError)
      )
  }

  updateDispositivo(dispositivo: Dispositivo): Observable<Dispositivo> {
    return this.httpClient.post<Dispositivo>(this.url + 'update_device', JSON.stringify(dispositivo), this.httpOptions)
      .pipe(
        retry(1),
        catchError(this.handleError)
      )
  }

  getCategoria(): Observable<Categoria[]> {
    return this.httpClient.get<Categoria[]>(this.url + 'list_category')
      .pipe(
        retry(2),
        catchError(this.handleError));
  }

  

  // Manipulação de erros
  handleError(error: HttpErrorResponse) {
    let errorMessage = '';
    if (error.error instanceof ErrorEvent) {
      // Erro ocorreu no lado do client
      errorMessage = error.error.message;
    } else {
      // Erro ocorreu no lado do servidor
      errorMessage = `Código do erro: ${error.status}, ` + `menssagem: ${error.message}`;
    }
    console.log(errorMessage);
    return throwError(errorMessage);
  };
  
  /************************************************************************************** */
  
  listarTodos(): Dispositivo[] {
    const dispositivos = localStorage['dispositivos'];
    return dispositivos ? JSON.parse(dispositivos) : [];
  }
  

  cadastrar(dispositivo: Dispositivo): void {
    const dispositivos = this.listarTodos();
    dispositivo.id = new Date().getTime();
    dispositivos.push(dispositivo);
    localStorage['dispositivos'] = JSON.stringify(dispositivos);
  }

  buscarPorId(id: number): Dispositivo {
    const dispositivos: Dispositivo[] = this.listarTodos();
    return dispositivos.find(dispositivo => dispositivo.id === id);    
  }

  atualizar(dispositivo: Dispositivo): void {
    console.log('atualizar');
    const dispositivos: Dispositivo[] = this.listarTodos();
    dispositivos.forEach((obj, index, objs) =>{      
      if (dispositivo.id === obj.id){        
        objs[index] = dispositivo;
      }
    });
    localStorage['dispositivos'] = JSON.stringify(dispositivos);

  }

  remover(id: number): void {
    console.log('remover');
    let dispositivos: Dispositivo[] = this.listarTodos();
    dispositivos = dispositivos.filter(dispositivo => dispositivo.id !== id);
    localStorage['dispositivos'] = JSON.stringify(dispositivos)
    
  }
  
}
