export class Dispositivo {
    constructor(
        public id?: number,
        public idCategory?: number,
        public color?: string,
        public partNumber?: string
    ){}
}
