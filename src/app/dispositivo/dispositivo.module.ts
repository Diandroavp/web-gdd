import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { RouterModule } from '@angular/router';
import { FormsModule } from '@angular/forms';

import { HttpClientModule } from '@angular/common/http';
import { DispositivoService } from './shared';
import { ListarDispositivoComponent } from './listar';
import { CadastrarDispositivoComponent } from './cadastrar';
import { EditarDispositivoComponent } from './editar';



@NgModule({  
  declarations: [
    ListarDispositivoComponent,
    CadastrarDispositivoComponent,
    EditarDispositivoComponent
  ],
  imports: [
    CommonModule,
    RouterModule,
    FormsModule,
    HttpClientModule
  ],  
  providers:[
    DispositivoService
  ]
})
export class DispositivoModule { }
