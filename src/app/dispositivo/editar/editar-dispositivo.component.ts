import { Component, OnInit, ViewChild } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { NgForm } from '@angular/forms';
import { Observable } from 'rxjs';

import { DispositivoService, Dispositivo } from '../shared';
import { CategoriaService, Categoria } from '../../categoria/shared';

@Component({
  selector: 'app-editar-dispositivo',
  templateUrl: './editar-dispositivo.component.html',
  styleUrls: ['./editar-dispositivo.component.css']
})
export class EditarDispositivoComponent implements OnInit {

  @ViewChild('formDispositivo', {static: true}) formDispositivo: NgForm;
  dispositivo: Dispositivo;
  categorias: Categoria[];

  constructor(
    private dispositivoService: DispositivoService,
    private categoriaService: CategoriaService,
    private route: ActivatedRoute,
    private router: Router
  ) { }

  ngOnInit(): void {
    this.dispositivo = new Dispositivo;
    const id = +this.route.snapshot.params['id'];        
    this.getDispositivoPorId(id);   
    this.getCategoria(); 
  }

  getDispositivoPorId(id: number){
    this.dispositivoService.getDispositivoId(id).subscribe((dispositivo: Dispositivo[]) => {      
      if (dispositivo['code'] == 200) {        
        this.dispositivo = dispositivo['data'][0];  
      } 
    });    
  }

  getCategoria(){        
    this.categoriaService.getCategoria().subscribe((categoria: Categoria[]) => {      
      if (categoria['code'] == 200) {        
        this.categorias = categoria['data'];        
      } else {
        console.log('code: ', categoria['code']);
        this.categorias = [];
      }
    });    
  }

  saveDispositivo() {
    if (this.dispositivo.id !== undefined) {
      this.dispositivoService.updateDispositivo(this.dispositivo).subscribe(() => {        
      });
    } else {
      this.dispositivoService.saveDispositivo(this.dispositivo).subscribe(() => {        
      });
    }
  }

  atualizar(): void {
    if (this.formDispositivo.form.valid) {
      this.saveDispositivo();   
      this.router.navigate(["/dispositivos"]);
    }
  }

}
