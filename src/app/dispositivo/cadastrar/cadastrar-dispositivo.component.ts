import { Component, OnInit, ViewChild } from '@angular/core';
import { Router } from '@angular/router';
import { NgForm } from '@angular/forms';

import { DispositivoService, Dispositivo } from '../shared';
import { CategoriaService, Categoria } from '../../categoria/shared';

@Component({
  selector: 'app-cadastrar-dispositivo',
  templateUrl: './cadastrar-dispositivo.component.html',
  styleUrls: ['./cadastrar-dispositivo.component.css']
})
export class CadastrarDispositivoComponent implements OnInit {

  @ViewChild('formDispositivo', {static: true}) formDispositivo: NgForm;
  dispositivo: Dispositivo;
  categorias: Categoria[];

  constructor(
    private dispositivoService: DispositivoService,
    private categoriaService: CategoriaService,
    private router: Router
  ) { }

  ngOnInit(): void {
    this.dispositivo   =  new Dispositivo;
    this.getCategoria();
  }

  saveDispositivo() {

    console.log('Dispo:' , this.dispositivo);
    if (this.dispositivo.id !== undefined) {
      this.dispositivoService.updateDispositivo(this.dispositivo).subscribe(() => {        
      });
    } else {
      this.dispositivoService.saveDispositivo(this.dispositivo).subscribe(() => {        
      });
    }
  }

  getCategoria(){        
    this.categoriaService.getCategoria().subscribe((categoria: Categoria[]) => {      
      if (categoria['code'] == 200) {        
        this.categorias = categoria['data'];        
      } else {
        console.log('code: ', categoria['code']);
        this.categorias = [];
      }
    });    
  }
  
  cadastrar(): void {
    if (this.formDispositivo.form.valid) {      
      this.saveDispositivo();
      this.router.navigate(["/dispositivos"]);
    }
  }
  

}
