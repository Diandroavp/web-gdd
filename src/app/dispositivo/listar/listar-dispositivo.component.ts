import { Component, OnInit } from '@angular/core';
import { Observable } from 'rxjs';
import { DispositivoService, Dispositivo } from '..';


@Component({
  selector: 'app-listar-dispositivo',
  templateUrl: './listar-dispositivo.component.html',
  styleUrls: ['./listar-dispositivo.component.css']
})
export class ListarDispositivoComponent implements OnInit {

  dispositivos: Dispositivo[];

  constructor(private dispositivoService: DispositivoService) { }

  ngOnInit(): void {           
    this.getDispositivo();
  }

  
  getDispositivo(){        
    this.dispositivoService.getDispositivo().subscribe((dispositivo: Dispositivo[]) => {      
      if (dispositivo['code'] == 200) {        
        this.dispositivos = dispositivo['data'];        
      } else {
        console.log('code: ', dispositivo['code']);
        this.dispositivos = [];
      }
    });    
  }

  remover($event: any, dispositivo: Dispositivo): void {
    $event.preventDefault();
    if (confirm('Confirma a remoção do cadastro "' +  dispositivo.partNumber + '"?')) {      
      this.dispositivoService.deleteDispositivo(dispositivo).subscribe(() => {
        this.getDispositivo();
      });      
    }
  }


  /*********** ACESSO EXTERNO */
  /*
  listarTodos(): Dispositivo[] {
    return this.dispositivoService.listarTodos();
    this.getDispositivo();    
    return this.dispositivos;
        
  }
*/
/*
  remover($event: any, dispositivo: Dispositivo): void {
    $event.preventDefault();
    if (confirm('Confirma a remoção do cadastro "' +  dispositivo.name + '"?')) {
      this.dispositivoService.remover(dispositivo.id);
      this.getDispositivo();
    }
  }
*/
}
