export * from './dispositivo.module';
export * from './shared';
export * from './listar';
export * from './cadastrar';
export * from './editar';
export * from './dispositivo-routing.module';
