import { Routes } from '@angular/router';
import { combineLatestInit } from 'rxjs/internal/observable/combineLatest';

import { ListarDispositivoComponent } from './listar';
import { CadastrarDispositivoComponent } from './cadastrar';
import { EditarDispositivoComponent } from './editar';

export const DispositivoRouts: Routes = [
    {
        path: 'dispositivos',
        redirectTo: 'dispositivos/listar'
    },
    {
        path: 'dispositivos/listar',
        component: ListarDispositivoComponent
    },
    {
        path: 'dispositivos/cadastrar',
        component: CadastrarDispositivoComponent
    },
    {
        path: 'dispositivos/editar/:id',
        component: EditarDispositivoComponent
    },
];

